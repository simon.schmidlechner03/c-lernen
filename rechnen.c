/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

int main()
{
    int zahl1;
    int zahl2;
    int ergebnis;

    printf("zahl1: ");
    scanf("%i", &zahl1);
    
    printf("zahl2: ");
    scanf("%i", &zahl2);
    
    ergebnis = zahl1 / zahl2;
    
    printf("\nDas Ergebnis ist: %i", ergebnis);

    return 0;
}
