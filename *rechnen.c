/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

int main()
{
    int zahl1;
    int zahl2;
    int product;
    printf("zahl1:");
    scanf("%i", &zahl1);
    
    printf("zahl2:");
    scanf("%i", &zahl2);
    
    product = zahl1 * zahl2;
    
    printf("das product der zahlen ist : %i", product);

    return 0;
}


