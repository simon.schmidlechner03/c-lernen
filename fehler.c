/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

int main()
{
    int zahl1;
    int zahl2;
    int zahl3;
    int ergebnis;
    
    printf("zahl1:");
    scanf("%i", zahl1);
    
    printf("zahl2:");
    scanf("%i", zahl2);
    
    printf("zahl3:");
    scanf("%i", zahl3);
    
    ergebnis = zahl1 * zahl2 - zahl3;
    
    printf("Das Ergebnis ist: %i", ergebnis);
    
    return 0;
}


