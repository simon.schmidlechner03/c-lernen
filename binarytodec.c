#include <stdio.h>

int main()
{
    int bit0;
    int bit1;
    int bit2;
    int bit3;
    int bit4;
    int bit5;
    int bit6;
    int bit7;
    int ergebnis;
    
    printf("bit0: ");
    scanf("%i", &bit0);

    printf("bit1: ");
    scanf("%i", &bit1);
    
    printf("bit2: ");
    scanf("%i", &bit2);
    
    printf("bit3: ");
    scanf("%i", &bit3);
    
    printf("bit4: ");
    scanf("%i", &bit4);

    printf("bit5: ");
    scanf("%i", &bit5);
    
    printf("bit6: ");
    scanf("%i", &bit6);
    
    printf("bit7: ");
    scanf("%i", &bit7);
    
    ergebnis = bit0 * 1 + bit1 * 2 + bit2 * 4 + bit3 * 8 + bit4 * 16 + bit5 *32 + bit6 * 64 + bit7 * 128;
    
    printf("\nDas dezimale ergebnis ist %d", ergebnis);
    printf("\nDas hexadezimale ergebnis ist %x", ergebnis);

    return 0;
}