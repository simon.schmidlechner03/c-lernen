#include <stdio.h>

int main()
{
    int tobias;
    int simon;
    int summe;
    
    printf("schreibe die lieblingszahl von tobias: " );
    scanf("%i", &tobias);
    printf("schreibe die lieblingszahl von simon: ");
    scanf("%i", &simon);
    
    summe = simon + tobias;
    printf("die summe von simons und tobias lieblingszahlen ist %i", summe);

    return 0;
}